import axios from 'axios'
import { handleAxiosError } from '../helpers'

// const locals = ['localhost']
// const hostname = document.location.hostname
// const local = locals.includes(hostname)
// const isServer = hostname === '192.168.2.254'
// const pcJP = window.localStorage.getItem('pcJP')
export default async ({ Vue, store }) => {
  // axios.defaults.baseURL = `http://${hostnmae}/iae/index.php?r=apiBuffet/`
  axios.defaults.baseURL = (process.env.NODE_ENV === 'production')
    ? 'https://iae.com.ar:3000/'
    : 'http://localhost:8888'
  // axios.defaults.baseURL = `https://iae.com.ar:3000/`
  axios.interceptors.response.use((response) => {
    const endPoint = response.config.url.substring(response.config.url.lastIndexOf('/') + 1)
    console.log('endpoint %O response %O', endPoint, response)
    if (response.status !== 200) throw handleAxiosError(response)
    if (response.headers.token) {
      store.dispatch('session/updateApiTokenFromInterceptor', response.headers.token)
    }
    response.data.status = response.status
    return response.data
  }, (error, b) => {
    Promise.reject(error)
  })

  axios.interceptors.request.use(
    async (config) => {
      config.headers.token = store.state.session.apiToken
      return config
    },
    error => Promise.reject(error)
  )

  axios.defaults.method = 'POST'
  axios.defaults.validateStatus = function (status) {
    return status >= 200 && status <= 503
  }
  Vue.prototype.$axios = axios
}
