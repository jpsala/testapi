import Vue from 'vue'

import store from 'src/store'
const articulosParaSubir = () => {
  return store.state.buffet.ticket.items.map((a) => {
    return {
      'id': a.id,
      'nombre': a.nombre,
      'subNombre': a.subNombre,
      'subId': a.subId,
      'precio_venta': a.precio_venta,
      'cantidad': a.cantidad
    }
  })
}
const assignSocio = (item, tipoItem, filter) => {
  store.dispatch('buffet/ticket/asignaSocio', item)
  // tipoItem.value = ''
  // filter.value = ''
}
const close = ({ cancela = true } = {}) => {
  store.dispatch('buffet/ticket/cierra', { cancela })
}
const print = async (importe) => {
  store.dispatch('addLoading', 'cierra-ticket', { root: true })
  const total = store.getters['buffet/ticket/total']
  return Vue.prototype.$axios.post('/cierra', {
    doc: {
      cliente: store.state.buffet.ticket.cliente,
      neto: store.getters['buffet/ticket/neto'],
      total,
      pago: importe,
      descuento: store.getters['buffet/ticket/descuento']
    },
    empleado_id: store.state.session.user.id,
    items: articulosParaSubir(),
    es_buffet: true
  }).then(async (resp) => {
    // actualizaCantidadesLuegoDeImprimir()
    await store.dispatch('buffet/ticket/cierra')
    return 'ok'
  }).catch(function (error) {
    return error
  }).finally(r => {
    store.dispatch('removeLoading', 'cierra-ticket', { root: true })
  })
}
const addArticulo = (articulo) => {
  store.dispatch('buffet/ticket/articuloAdd', articulo)
}
const delArticulo = (articulo) => {
  store.dispatch('buffet/ticket/articuloDelete', articulo)
}
const articuloAddCant = (itemIndex, cantidad) => {
  store.dispatch('buffet/ticket/articuloAddCant', { item: store.state.buffet.ticket.items[itemIndex], cantidad })
}
const articuloModPrecio = (item, precio) => {
  store.dispatch('buffet/ticket/articuloModPrecio', { item, precio })
}
const articuloSetCant = (item, cantidad) => {
  store.dispatch('buffet/ticket/articuloSetCant', { item, cantidad })
}
export { assignSocio, close, print, addArticulo, delArticulo, articuloAddCant, articuloSetCant, articuloModPrecio }
