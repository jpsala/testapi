import Vue from 'vue'
// import { reactive } from '@vue/composition-api'
const getSocios = (filtro) => {
  return Vue.prototype.$axios.post('/socios', {
    query: filtro,
    'con_empleados': true
  }).then((resp) => {
    return Number(resp.data)// resp.data.map(socio => Object.assign({}, socio)
  })
}
const getDeudores = async () => {
  return Vue.prototype.$axios.post('/deudores').then((resp) => {
    console.log('resp', resp.data)
    return resp.data
  })
}
const getSociosFiltrados = async (filtro = '') => {
  // if (filtro.length < 3) {
  //   return []
  // }
  const filtrados = await getSocios(filtro)
  // filtrados = [{ id: 1, nombre: 'juan', precio_venta: 12 }]

  return filtrados
}
const getSaldo = async (socioId) => {
  return Vue.prototype.$axios.post('/saldoSocio', { socio_id: socioId }).then(r => Number(r.saldo))
}
const getCta = async ({ id, todos = false }) => {
  let total = 0
  let saldo = 0
  const resp = await Vue.prototype.$axios.post('/estadoDeCuenta', { socio_id: id, todos })
  const data = resp.data.map(d => {
    d.total = parseFloat(d.total)
    d.saldo = parseFloat(d.saldo)
    total += d.saldo
    saldo = total
    return d
  })
  return {
    data,
    total,
    saldo
  }
}
export { getSociosFiltrados, getCta, getSaldo, getDeudores }
