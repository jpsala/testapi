import Vue from 'vue'
import store from 'src/store'
import colors from 'src/colors'

const getProductosYCategorias = async (ctx) => {
  const resp = await Vue.prototype.$axios.get('articulos')
  const categorias = resp.data.categorias
  const productos = resp.data.articulos.filter(art => {
    return art.disponible === '1'
  })
  return { categorias, productos }
}
const getCategorias = async () => {
  const { productos, categorias } = await getProductosYCategorias()
  return categorias.map((categoria, index) => {
    const productosDeEstaCategoria = productos.filter(prod => {
      // console.log('prod.categoria === categoria.id', prod.categoria_id, categoria.id)
      prod.categoria_nombre = categoria.nombre
      return prod.categoria_id === categoria.id
    })
    categoria.productos = productosDeEstaCategoria
    return categoria
  })
}
const getProductos = async (ctx) => {
  const resp = await Vue.prototype.$axios.get('articulos')
  const coloresUsados = []
  let random
  const getRandomColor = () => Math.floor(Math.random() * colors.length - 1 + 0)
  const categorias = resp.data.categorias.map(c => {
    random = getRandomColor()
    while (coloresUsados.includes(random)) {
      random = getRandomColor()
    }
    c.color = colors[random]
    coloresUsados.push(random)
    return c
  })
  const articulos = resp.data.articulos.map(a => {
    const categoria = categorias.find(c => c.id === a.categoria_id)
    a.color = categoria.color
    return a
  })
  store.commit('buffet/SET_CATEGORIAS', categorias)
  store.commit('buffet/SET_ARTICULOS', articulos)
}
const productoNuevo = {
  id: -1,
  nombre: '',
  precio_venta: 0,
  disponible: '1'
}
const grabaProducto = async (producto) => {
  return Vue.prototype.$axios.post('grabaArticulo', producto)
}
export { getCategorias, productoNuevo, grabaProducto, getProductos }
