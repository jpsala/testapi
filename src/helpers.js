const soloEnDevMode = func => {
  if (process.env.NODE_ENV === 'development') func()
}
const log = (...args) => {
  if (!process.env.NODE_ENV === 'development') return
  console.warn(...args)
  console.trace()
}
const handleAxiosError = (_error) => {
  const error = {}
  if (!_error) error.statusMsg = 'Error'
  else if (_error.data && _error.data.message) error.statusMsg = _error.data.message
  if (_error.status) error.status = _error.status
  return error
}
export { soloEnDevMode, log, handleAxiosError }
